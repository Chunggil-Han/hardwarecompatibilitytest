namespace HardwareCompatibilityTest
{
    partial class TerminalDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ReplaceTerminal = new System.Windows.Forms.Button();
            this.compatibilitytestOnhardware = new System.Windows.Forms.Button();
            this.groupBoxTerminalConfig = new System.Windows.Forms.GroupBox();
            this.btnShowTerminalDetails = new System.Windows.Forms.Button();
            this.labelTextTerminalID = new System.Windows.Forms.Label();
            this.comboBoxTerminalID = new System.Windows.Forms.ComboBox();
            this.labelDownloadingData = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupBoxTerminalConfig.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // ReplaceTerminal
            // 
            this.ReplaceTerminal.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ReplaceTerminal.Location = new System.Drawing.Point(101, 61);
            this.ReplaceTerminal.Name = "ReplaceTerminal";
            this.ReplaceTerminal.Size = new System.Drawing.Size(286, 29);
            this.ReplaceTerminal.TabIndex = 0;
            this.ReplaceTerminal.Text = "Show all POS terminals";
            this.ReplaceTerminal.UseVisualStyleBackColor = false;
            this.ReplaceTerminal.Click += new System.EventHandler(this.ReplaceTerminal_Click_1);
            // 
            // compatibilitytestOnhardware
            // 
            this.compatibilitytestOnhardware.BackColor = System.Drawing.Color.CornflowerBlue;
            this.compatibilitytestOnhardware.Location = new System.Drawing.Point(101, 121);
            this.compatibilitytestOnhardware.Name = "compatibilitytestOnhardware";
            this.compatibilitytestOnhardware.Size = new System.Drawing.Size(286, 29);
            this.compatibilitytestOnhardware.TabIndex = 1;
            this.compatibilitytestOnhardware.Text = "Perform a compatibility test on your hardware";
            this.compatibilitytestOnhardware.UseVisualStyleBackColor = false;
            this.compatibilitytestOnhardware.Click += new System.EventHandler(this.compatibilitytestOnhardware_Click_1);
            // 
            // groupBoxTerminalConfig
            // 
            this.groupBoxTerminalConfig.Controls.Add(this.btnShowTerminalDetails);
            this.groupBoxTerminalConfig.Controls.Add(this.labelTextTerminalID);
            this.groupBoxTerminalConfig.Controls.Add(this.comboBoxTerminalID);
            this.groupBoxTerminalConfig.Location = new System.Drawing.Point(37, 187);
            this.groupBoxTerminalConfig.Name = "groupBoxTerminalConfig";
            this.groupBoxTerminalConfig.Size = new System.Drawing.Size(416, 138);
            this.groupBoxTerminalConfig.TabIndex = 2;
            this.groupBoxTerminalConfig.TabStop = false;
            this.groupBoxTerminalConfig.Text = "Unassigned Terminals";
            // 
            // btnShowTerminalDetails
            // 
            this.btnShowTerminalDetails.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnShowTerminalDetails.Location = new System.Drawing.Point(304, 96);
            this.btnShowTerminalDetails.Name = "btnShowTerminalDetails";
            this.btnShowTerminalDetails.Size = new System.Drawing.Size(98, 26);
            this.btnShowTerminalDetails.TabIndex = 2;
            this.btnShowTerminalDetails.Text = "&Show";
            this.btnShowTerminalDetails.UseVisualStyleBackColor = false;
            this.btnShowTerminalDetails.Click += new System.EventHandler(this.btnShowTerminalDetails_Click_1);
            // 
            // labelTextTerminalID
            // 
            this.labelTextTerminalID.AutoSize = true;
            this.labelTextTerminalID.Location = new System.Drawing.Point(40, 55);
            this.labelTextTerminalID.Name = "labelTextTerminalID";
            this.labelTextTerminalID.Size = new System.Drawing.Size(64, 13);
            this.labelTextTerminalID.TabIndex = 1;
            this.labelTextTerminalID.Text = "Terminal ID:";
            // 
            // comboBoxTerminalID
            // 
            this.comboBoxTerminalID.FormattingEnabled = true;
            this.comboBoxTerminalID.Location = new System.Drawing.Point(176, 55);
            this.comboBoxTerminalID.Name = "comboBoxTerminalID";
            this.comboBoxTerminalID.Size = new System.Drawing.Size(205, 21);
            this.comboBoxTerminalID.TabIndex = 0;
            this.comboBoxTerminalID.Text = "l";
            // 
            // labelDownloadingData
            // 
            this.labelDownloadingData.AutoSize = true;
            this.labelDownloadingData.Location = new System.Drawing.Point(98, 187);
            this.labelDownloadingData.Name = "labelDownloadingData";
            this.labelDownloadingData.Size = new System.Drawing.Size(0, 13);
            this.labelDownloadingData.TabIndex = 3;
            this.labelDownloadingData.Click += new System.EventHandler(this.labelDownloadingData_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_Image;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(-2, -4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(143, 50);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_BackgroundImage;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(-2, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(498, 46);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_BackgroundImage;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(-2, 346);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(498, 50);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // TerminalDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(496, 392);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.labelDownloadingData);
            this.Controls.Add(this.groupBoxTerminalConfig);
            this.Controls.Add(this.compatibilitytestOnhardware);
            this.Controls.Add(this.ReplaceTerminal);
            this.Name = "TerminalDetails";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TerminalDetails";
            this.Load += new System.EventHandler(this.TerminalDetails_Load);
            this.groupBoxTerminalConfig.ResumeLayout(false);
            this.groupBoxTerminalConfig.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ReplaceTerminal;
        private System.Windows.Forms.Button compatibilitytestOnhardware;
        private System.Windows.Forms.GroupBox groupBoxTerminalConfig;
        private System.Windows.Forms.Button btnShowTerminalDetails;
        private System.Windows.Forms.Label labelTextTerminalID;
        private System.Windows.Forms.ComboBox comboBoxTerminalID;
        private System.Windows.Forms.Label labelDownloadingData;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}