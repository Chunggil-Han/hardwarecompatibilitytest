namespace HardwareCompatibilityTest
{
    partial class HardwareConfig
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GroupReceiptPrinter = new System.Windows.Forms.GroupBox();
            this.comboBoxReceiptPrinterPort = new System.Windows.Forms.ComboBox();
            this.BtnPrintSampleReceipt = new System.Windows.Forms.Button();
            this.comboReceiptPrinter = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.GroupCusromerDisplay = new System.Windows.Forms.GroupBox();
            this.comboBoxCustomerDisplayPort = new System.Windows.Forms.ComboBox();
            this.btnDisplaySample = new System.Windows.Forms.Button();
            this.comboBoxCustomerDisplay = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupInvoicePrinter = new System.Windows.Forms.GroupBox();
            this.comboBoxInvoicePrinterPort = new System.Windows.Forms.ComboBox();
            this.btnPrintInvoiceSample = new System.Windows.Forms.Button();
            this.comboBoxInvoicePrinter = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBarcode = new System.Windows.Forms.GroupBox();
            this.comboBoxBarcodePrinterPort = new System.Windows.Forms.ComboBox();
            this.btnPrintBarcode = new System.Windows.Forms.Button();
            this.comboBoxBarcodePrinter = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.GroupReceiptPrinter.SuspendLayout();
            this.GroupCusromerDisplay.SuspendLayout();
            this.groupInvoicePrinter.SuspendLayout();
            this.groupBarcode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // GroupReceiptPrinter
            // 
            this.GroupReceiptPrinter.Controls.Add(this.comboBoxReceiptPrinterPort);
            this.GroupReceiptPrinter.Controls.Add(this.BtnPrintSampleReceipt);
            this.GroupReceiptPrinter.Controls.Add(this.comboReceiptPrinter);
            this.GroupReceiptPrinter.Controls.Add(this.label1);
            this.GroupReceiptPrinter.Controls.Add(this.label2);
            this.GroupReceiptPrinter.Location = new System.Drawing.Point(41, 55);
            this.GroupReceiptPrinter.Name = "GroupReceiptPrinter";
            this.GroupReceiptPrinter.Size = new System.Drawing.Size(518, 100);
            this.GroupReceiptPrinter.TabIndex = 0;
            this.GroupReceiptPrinter.TabStop = false;
            this.GroupReceiptPrinter.Text = "Receipt Printer";
            // 
            // comboBoxReceiptPrinterPort
            // 
            this.comboBoxReceiptPrinterPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxReceiptPrinterPort.FormattingEnabled = true;
            this.comboBoxReceiptPrinterPort.Location = new System.Drawing.Point(170, 59);
            this.comboBoxReceiptPrinterPort.Name = "comboBoxReceiptPrinterPort";
            this.comboBoxReceiptPrinterPort.Size = new System.Drawing.Size(240, 21);
            this.comboBoxReceiptPrinterPort.TabIndex = 15;
            // 
            // BtnPrintSampleReceipt
            // 
            this.BtnPrintSampleReceipt.BackColor = System.Drawing.Color.CornflowerBlue;
            this.BtnPrintSampleReceipt.Location = new System.Drawing.Point(427, 38);
            this.BtnPrintSampleReceipt.Name = "BtnPrintSampleReceipt";
            this.BtnPrintSampleReceipt.Size = new System.Drawing.Size(85, 23);
            this.BtnPrintSampleReceipt.TabIndex = 1;
            this.BtnPrintSampleReceipt.Text = "Print Sample";
            this.BtnPrintSampleReceipt.UseVisualStyleBackColor = false;
            this.BtnPrintSampleReceipt.Click += new System.EventHandler(this.BtnPrintSampleReceipt_Click_1);
            // 
            // comboReceiptPrinter
            // 
            this.comboReceiptPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboReceiptPrinter.FormattingEnabled = true;
            this.comboReceiptPrinter.Items.AddRange(new object[] {
            "Cash Drawer With No Receipt Printer Attached",
            "Default Printer",
            "Epson Printer",
            "Epson TM-T88III",
            "Generic Printer with no cash register",
            "ithaca POSJet 1000",
            "NCR 7194",
            "No Printer Attached",
            "Star Printer"});
            this.comboReceiptPrinter.Location = new System.Drawing.Point(170, 19);
            this.comboReceiptPrinter.Name = "comboReceiptPrinter";
            this.comboReceiptPrinter.Size = new System.Drawing.Size(240, 21);
            this.comboReceiptPrinter.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Receipt Printer:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(105, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Receipt Printer Port: ";
            // 
            // GroupCusromerDisplay
            // 
            this.GroupCusromerDisplay.Controls.Add(this.comboBoxCustomerDisplayPort);
            this.GroupCusromerDisplay.Controls.Add(this.btnDisplaySample);
            this.GroupCusromerDisplay.Controls.Add(this.comboBoxCustomerDisplay);
            this.GroupCusromerDisplay.Controls.Add(this.label3);
            this.GroupCusromerDisplay.Controls.Add(this.label4);
            this.GroupCusromerDisplay.Location = new System.Drawing.Point(41, 161);
            this.GroupCusromerDisplay.Name = "GroupCusromerDisplay";
            this.GroupCusromerDisplay.Size = new System.Drawing.Size(518, 100);
            this.GroupCusromerDisplay.TabIndex = 0;
            this.GroupCusromerDisplay.TabStop = false;
            this.GroupCusromerDisplay.Text = "Cusromer Display";
            // 
            // comboBoxCustomerDisplayPort
            // 
            this.comboBoxCustomerDisplayPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCustomerDisplayPort.FormattingEnabled = true;
            this.comboBoxCustomerDisplayPort.Location = new System.Drawing.Point(170, 57);
            this.comboBoxCustomerDisplayPort.Name = "comboBoxCustomerDisplayPort";
            this.comboBoxCustomerDisplayPort.Size = new System.Drawing.Size(240, 21);
            this.comboBoxCustomerDisplayPort.TabIndex = 13;
            // 
            // btnDisplaySample
            // 
            this.btnDisplaySample.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnDisplaySample.Location = new System.Drawing.Point(427, 39);
            this.btnDisplaySample.Name = "btnDisplaySample";
            this.btnDisplaySample.Size = new System.Drawing.Size(85, 23);
            this.btnDisplaySample.TabIndex = 2;
            this.btnDisplaySample.Text = "Display Sample";
            this.btnDisplaySample.UseVisualStyleBackColor = false;
            this.btnDisplaySample.Click += new System.EventHandler(this.btnDisplaySample_Click_1);
            // 
            // comboBoxCustomerDisplay
            // 
            this.comboBoxCustomerDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCustomerDisplay.FormattingEnabled = true;
            this.comboBoxCustomerDisplay.Location = new System.Drawing.Point(170, 19);
            this.comboBoxCustomerDisplay.Name = "comboBoxCustomerDisplay";
            this.comboBoxCustomerDisplay.Size = new System.Drawing.Size(240, 21);
            this.comboBoxCustomerDisplay.TabIndex = 14;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Customer Display:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 65);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 13);
            this.label4.TabIndex = 4;
            this.label4.Text = "Customer Display Port: ";
            // 
            // groupInvoicePrinter
            // 
            this.groupInvoicePrinter.Controls.Add(this.comboBoxInvoicePrinterPort);
            this.groupInvoicePrinter.Controls.Add(this.btnPrintInvoiceSample);
            this.groupInvoicePrinter.Controls.Add(this.comboBoxInvoicePrinter);
            this.groupInvoicePrinter.Controls.Add(this.label5);
            this.groupInvoicePrinter.Controls.Add(this.label6);
            this.groupInvoicePrinter.Location = new System.Drawing.Point(41, 267);
            this.groupInvoicePrinter.Name = "groupInvoicePrinter";
            this.groupInvoicePrinter.Size = new System.Drawing.Size(518, 100);
            this.groupInvoicePrinter.TabIndex = 0;
            this.groupInvoicePrinter.TabStop = false;
            this.groupInvoicePrinter.Text = "Invoice Printer";
            // 
            // comboBoxInvoicePrinterPort
            // 
            this.comboBoxInvoicePrinterPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInvoicePrinterPort.FormattingEnabled = true;
            this.comboBoxInvoicePrinterPort.Location = new System.Drawing.Point(170, 59);
            this.comboBoxInvoicePrinterPort.Name = "comboBoxInvoicePrinterPort";
            this.comboBoxInvoicePrinterPort.Size = new System.Drawing.Size(240, 21);
            this.comboBoxInvoicePrinterPort.TabIndex = 11;
            // 
            // btnPrintInvoiceSample
            // 
            this.btnPrintInvoiceSample.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnPrintInvoiceSample.Location = new System.Drawing.Point(427, 44);
            this.btnPrintInvoiceSample.Name = "btnPrintInvoiceSample";
            this.btnPrintInvoiceSample.Size = new System.Drawing.Size(85, 23);
            this.btnPrintInvoiceSample.TabIndex = 3;
            this.btnPrintInvoiceSample.Text = "Print Sample";
            this.btnPrintInvoiceSample.UseVisualStyleBackColor = false;
            this.btnPrintInvoiceSample.Click += new System.EventHandler(this.btnPrintInvoiceSample_Click_1);
            // 
            // comboBoxInvoicePrinter
            // 
            this.comboBoxInvoicePrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInvoicePrinter.FormattingEnabled = true;
            this.comboBoxInvoicePrinter.Location = new System.Drawing.Point(170, 23);
            this.comboBoxInvoicePrinter.Name = "comboBoxInvoicePrinter";
            this.comboBoxInvoicePrinter.Size = new System.Drawing.Size(240, 21);
            this.comboBoxInvoicePrinter.TabIndex = 12;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(20, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Invoice Printer: ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 67);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(103, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "Invoice Printer Port: ";
            // 
            // groupBarcode
            // 
            this.groupBarcode.Controls.Add(this.comboBoxBarcodePrinterPort);
            this.groupBarcode.Controls.Add(this.btnPrintBarcode);
            this.groupBarcode.Controls.Add(this.comboBoxBarcodePrinter);
            this.groupBarcode.Controls.Add(this.label7);
            this.groupBarcode.Controls.Add(this.label8);
            this.groupBarcode.Location = new System.Drawing.Point(41, 375);
            this.groupBarcode.Name = "groupBarcode";
            this.groupBarcode.Size = new System.Drawing.Size(518, 100);
            this.groupBarcode.TabIndex = 0;
            this.groupBarcode.TabStop = false;
            this.groupBarcode.Text = "Barcode Printer";
            // 
            // comboBoxBarcodePrinterPort
            // 
            this.comboBoxBarcodePrinterPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBarcodePrinterPort.FormattingEnabled = true;
            this.comboBoxBarcodePrinterPort.Location = new System.Drawing.Point(170, 64);
            this.comboBoxBarcodePrinterPort.Name = "comboBoxBarcodePrinterPort";
            this.comboBoxBarcodePrinterPort.Size = new System.Drawing.Size(240, 21);
            this.comboBoxBarcodePrinterPort.TabIndex = 10;
            // 
            // btnPrintBarcode
            // 
            this.btnPrintBarcode.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnPrintBarcode.Location = new System.Drawing.Point(427, 42);
            this.btnPrintBarcode.Name = "btnPrintBarcode";
            this.btnPrintBarcode.Size = new System.Drawing.Size(85, 25);
            this.btnPrintBarcode.TabIndex = 4;
            this.btnPrintBarcode.Text = "Print Sample";
            this.btnPrintBarcode.UseVisualStyleBackColor = false;
            this.btnPrintBarcode.Click += new System.EventHandler(this.btnPrintBarcode_Click_1);
            // 
            // comboBoxBarcodePrinter
            // 
            this.comboBoxBarcodePrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxBarcodePrinter.FormattingEnabled = true;
            this.comboBoxBarcodePrinter.Location = new System.Drawing.Point(170, 24);
            this.comboBoxBarcodePrinter.Name = "comboBoxBarcodePrinter";
            this.comboBoxBarcodePrinter.Size = new System.Drawing.Size(240, 21);
            this.comboBoxBarcodePrinter.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(20, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(87, 13);
            this.label7.TabIndex = 7;
            this.label7.Text = "Bar Code Printer:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(20, 67);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(112, 13);
            this.label8.TabIndex = 8;
            this.label8.Text = "Bar Code Printer Port: ";
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(468, 492);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(106, 29);
            this.btnBack.TabIndex = 20;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_BackgroundImage;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(2, 481);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(607, 50);
            this.pictureBox3.TabIndex = 8;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_BackgroundImage;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(2, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(607, 49);
            this.pictureBox2.TabIndex = 7;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_Image;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(2, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(164, 49);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // HardwareConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(606, 533);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.GroupCusromerDisplay);
            this.Controls.Add(this.groupInvoicePrinter);
            this.Controls.Add(this.groupBarcode);
            this.Controls.Add(this.GroupReceiptPrinter);
            this.Name = "HardwareConfig";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HardwareConfig";
            this.GroupReceiptPrinter.ResumeLayout(false);
            this.GroupReceiptPrinter.PerformLayout();
            this.GroupCusromerDisplay.ResumeLayout(false);
            this.GroupCusromerDisplay.PerformLayout();
            this.groupInvoicePrinter.ResumeLayout(false);
            this.groupInvoicePrinter.PerformLayout();
            this.groupBarcode.ResumeLayout(false);
            this.groupBarcode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GroupReceiptPrinter;
        private System.Windows.Forms.ComboBox comboBoxReceiptPrinterPort;
        private System.Windows.Forms.ComboBox comboReceiptPrinter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox GroupCusromerDisplay;
        private System.Windows.Forms.ComboBox comboBoxCustomerDisplayPort;
        private System.Windows.Forms.ComboBox comboBoxCustomerDisplay;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupInvoicePrinter;
        private System.Windows.Forms.ComboBox comboBoxInvoicePrinterPort;
        private System.Windows.Forms.ComboBox comboBoxInvoicePrinter;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBarcode;
        private System.Windows.Forms.ComboBox comboBoxBarcodePrinterPort;
        private System.Windows.Forms.ComboBox comboBoxBarcodePrinter;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button BtnPrintSampleReceipt;
        private System.Windows.Forms.Button btnDisplaySample;
        private System.Windows.Forms.Button btnPrintInvoiceSample;
        private System.Windows.Forms.Button btnPrintBarcode;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}