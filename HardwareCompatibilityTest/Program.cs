﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Net;

namespace HardwareCompatibilityTest
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                //Application.Run(new Form1());
                Application.Run(new LoginControl());
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred.......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Message:" + ex.Message + " - Source:" + ex.Source);
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed.....please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }
    }
}
