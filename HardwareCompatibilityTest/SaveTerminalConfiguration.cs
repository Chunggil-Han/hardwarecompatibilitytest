using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ADODB;
using RealTime7Client.UpdateComponents;
using RealTime7.Proxy;
using Microsoft.Win32;
using RealTime7.Utilities;
//using System.Runtime.InteropServices;
//using System.Diagnostics;


namespace HardwareCompatibilityTest
{

    public partial class SaveTerminalConfiguration : Form
    {
    
        public void setStoreIDAndDefaultConfiguration(string TerminalID, Recordset DefaultConfig)
        {
            labelTerminalID.Text = TerminalID;

            if (DefaultConfig != null)
            {
                if (DefaultConfig.RecordCount > 0)
                {

                    //comboReceiptPrinter.SelectedIndex = Convert.ToInt32(DefaultConfig.Fields["PrinterType"].Value);
                    switch (Convert.ToInt32(DefaultConfig.Fields["PrinterType"].Value))
                    {
                        case 0:
                            comboReceiptPrinter.SelectedIndex = 7;
                            break;

                        case 1:
                            {
                                comboReceiptPrinter.SelectedIndex = 4;
                                break;
                            }
                        case 2:
                            comboReceiptPrinter.SelectedIndex = 8;
                            break;
                        case 3:
                            comboReceiptPrinter.SelectedIndex = 5;
                            break;
                        case 4:
                            comboReceiptPrinter.SelectedIndex = 1;
                            break;
                        case 5:
                            comboReceiptPrinter.SelectedIndex = 0;
                            break;
                        case 6:
                            comboReceiptPrinter.SelectedIndex = 0;
                            break;
                        case 7:
                            comboReceiptPrinter.SelectedIndex = 7;
                            break;
                        case 8:
                            comboReceiptPrinter.SelectedIndex = 8;
                            break;
                        case 9:
                            comboReceiptPrinter.SelectedIndex = 3;
                            break;
                        case 10:
                            comboReceiptPrinter.SelectedIndex = 6;
                            break;
                        case 11:
                            comboReceiptPrinter.SelectedIndex = 3;
                            break;
                        case 12:
                            comboReceiptPrinter.SelectedIndex = 7;
                            break;
                        default:
                            comboReceiptPrinter.SelectedIndex = 7;
                            break;


                    }

                    comboBoxReceiptPrinterPort.SelectedIndex = GetIndexFromString(DefaultConfig.Fields["PrinterPort"].Value.ToString());
                    comboBoxCustomerDisplay.SelectedIndex = Convert.ToInt32(DefaultConfig.Fields["CustomerDisplayType"].Value);
                    comboBoxCustomerDisplayPort.SelectedIndex = GetIndexFromString(DefaultConfig.Fields["CustomerDisplayPort"].Value.ToString());
                    comboBoxInvoicePrinter.SelectedIndex = Convert.ToInt32(DefaultConfig.Fields["InvoicePrinterType"].Value);
                    comboBoxInvoicePrinterPort.SelectedIndex = GetIndexFromString(DefaultConfig.Fields["InvoicePrinterPort"].Value.ToString());
                }
            }

        }

        private string[] WantedPrinter =
        { 
            "No Printer Attached", //       			0                        
            "Generic Printer with no cash register",//             	1
            "Star Printer",//                                      	              2     If USB:8
            "ithaca POSJet 1000",//          			3		                      
            "Epson Printer",//              			4
            "NCR 7194",//				7
            "Epson TM-T88III",//				11 if USB :9
            "Default Printer",// 				10
            "Cash Drawer With No Receipt Printer Attached"//   13

        };

        private int GetIndexFromString(string strPort)
        {
            switch (strPort)
            {
                case "LPT1:":
                    return 0;
                case "COM1:":
                    return 1;
                case "COM2:":
                    return 2;
                case "USB:":
                    return 3;
                default:
                    return 0;
            }
        }
        public SaveTerminalConfiguration(Recordset ReceiptPrintets, Recordset InvoicePrinters, Recordset PoleDisplay)
        {
            InitializeComponent();
            comboBoxReceiptPrinterPort.SelectedIndex = 0;
            comboBoxCustomerDisplayPort.SelectedIndex = 0;
            comboBoxInvoicePrinterPort.SelectedIndex = 0;

            if (ReceiptPrintets != null)
            {
                string[] strReceiptPrinters = new string[ReceiptPrintets.RecordCount];
                ReceiptPrintets.MoveFirst();
                for (int i = 0; i < ReceiptPrintets.RecordCount; i++)
                {
                    strReceiptPrinters[i] = ReceiptPrintets.Fields["PrinterName"].Value.ToString();
                    ReceiptPrintets.MoveNext();
                }
                // Add the Printers Selectively





                //comboReceiptPrinter.DataSource = strReceiptPrinters;
            }
            if (InvoicePrinters != null)
            {
                string[] strReceiptPrinters = new string[InvoicePrinters.RecordCount];
                InvoicePrinters.MoveFirst();
                for (int i = 0; i < InvoicePrinters.RecordCount; i++)
                {
                    strReceiptPrinters[i] = InvoicePrinters.Fields["InvoicePrinterName"].Value.ToString();
                    InvoicePrinters.MoveNext();
                }
                comboBoxInvoicePrinter.DataSource = strReceiptPrinters;
            }
            if (PoleDisplay != null)
            {
                string[] strReceiptPrinters = new string[PoleDisplay.RecordCount];
                PoleDisplay.MoveFirst();
                for (int i = 0; i < PoleDisplay.RecordCount; i++)
                {
                    strReceiptPrinters[i] = PoleDisplay.Fields["CustomerDisplayName"].Value.ToString();
                    PoleDisplay.MoveNext();
                }
                comboBoxCustomerDisplay.DataSource = strReceiptPrinters;

            }
        }


       
        private string PD2PN(string PortDescription)
        {
            switch (PortDescription)
            {
                case "Parallel Port (LPT1)":
                    return "LPT1:";
                case "Communications Port (COM1)":
                    return "COM1:";
                case "Communications Port (COM2)":
                    return "COM2:";
                case "USB Port":
                    return "USB:";
                default:
                    return "LPT1:";
            }
        }



        void UpdateComponent_HWModifyPOSConfigEvent(object source, ResultRSEventArgs e)
        {
            UpdateComponent.HWModifyPOSConfigEvent -= UpdateComponent_HWModifyPOSConfigEvent;
            if (e.retVal.retCode == enmResult.Success)
            {
                ADODB.Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                if (rsServerData.RecordCount > 0)
                {
                    //MessageBox.Show(rsServerData.Fields["AccessToken"].Value.ToString());
                    // Prasad:Notify The Installer -HW COnfiguration was ok
                    clsRegistry Objregistry = new clsRegistry();
                    Objregistry.SetStringValue(Registry.LocalMachine, @"Software\Mind The Store\POS", "HWConfig", "1");
                    //End Prasad

                    RegistryKey subKey = null;
                    //string strRegError = "";
                    try
                    {
                        subKey = Microsoft.Win32.Registry.LocalMachine.CreateSubKey(@"SOFTWARE\Mind The Store\POS");
                        if (subKey == null)
                        {
                            MessageBox.Show("Failed to set registry setting.....Please login with administrator access.");
                            this.Close();
                            // System.Windows.Forms.Application.Exit();
                            return;
                        }
                        subKey.SetValue("DeviceID", labelTerminalID.Text);
                        subKey.SetValue("ModelNumber", rsServerData.Fields["AccessToken"].Value.ToString());
                        subKey.SetValue("Configured", "1");
                        subKey.Close();
                        Microsoft.Win32.Registry.LocalMachine.Close();
                        //restartterminal();
                        //if (DialogResult.Yes == MessageBox.Show("The POS Terminal must be restarted in order for the configuration to take effect.\n                  Do you want to restart the terminal?", "Inventory", MessageBoxButtons.YesNo,MessageBoxIcon.Stop))
                        //{
                        //Computer_Restart();
                        //}
                        MessageBox.Show("Terminal configured successfully");
                       // System.Windows.Forms.Application.Exit();
                        this.Close();
                        
                        
                      
                        
                    }
                    catch (Exception exc)
                    {
                        MessageBox.Show("Error occurred during accessing registry settings.....Please login with administrator access." + exc.Message);
                        this.Close();
                        // System.Windows.Forms.Application.Exit();
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Failed to configure the terminal.");
                    this.Close();
                    //System.Windows.Forms.Application.Exit();
                }
            }
            else
            {
                MessageBox.Show("Failed to configure the terminal....Please try after few moments. ");
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }
        }

        private void comboReceiptPrinter_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

     
        private void SaveConfig_Click_1(object sender, EventArgs e)
        {
            int printerType = 0;
            switch (comboReceiptPrinter.Text)
            {
                /*
                     
                0   No Printer Attached                         0
                1   Generic Printer with no cash register       1
                2   Star Printer                                2 If USB:8
                3   ithaca POSJet 1000                          3
                4   Epson Printer                               4
                5   NCR 7194                                   7                               
                6   Epson TM-T88III                            11 If USB:9
                7   Default Printer                            10
                8   Cash Drawer With No Receipt Printer Attached 13
                     
                 */
                case "No Printer Attached":// No Printer Attached 
                    {
                        printerType = 0;
                        break;
                    }

                case "Generic Printer with no cash register"://Generic Printer with no cash register
                    {
                        printerType = 2;
                        break;
                    }
                case "Star Printer"://Star Printer 
                    {
                        if (comboBoxReceiptPrinterPort.Text == "USB Port")
                            printerType = 8;
                        else
                            printerType = 2;
                        break;
                    }

                case "ithaca POSJet 1000"://ithaca POSJet 1000 
                    {
                        printerType = 3;
                        break;
                    }

                case "Epson Printer"://Epson Printer  
                    {
                        printerType = 4;
                        break;
                    }

                case "NCR 7194":// NCR 7194  
                    {
                        printerType = 7;
                        break;
                    }

                case "Epson TM-T88III"://Epson TM-T88III 
                    {
                        if (comboBoxReceiptPrinterPort.Text == "USB Port")
                            printerType = 9;
                        else
                            printerType = 11;

                        break;
                    }

                case "Default Printer":// Default Printer 
                    {
                        printerType = 10;
                        break;
                    }

                case "Cash Drawer With No Receipt Printer Attached":
                    printerType = 13;
                    break;



                default:
                    printerType = 0;
                    break;


            }


            UpdateComponent.HWModifyPOSConfigEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWModifyPOSConfigEvent);

            UpdateComponent.HWModifyPOSConfig(labelTerminalID.Text, (short)printerType/* (short)comboReceiptPrinter.SelectedIndex*/, PD2PN(comboBoxReceiptPrinterPort.Text), (short)comboBoxCustomerDisplay.SelectedIndex, PD2PN(comboBoxCustomerDisplayPort.Text), (short)comboBoxInvoicePrinter.SelectedIndex, PD2PN(comboBoxInvoicePrinterPort.Text), true);
        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {
            
            TerminalDetails.ActiveForm.Show();
            this.Close();
        }

        private void comboReceiptPrinter_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

    }
}