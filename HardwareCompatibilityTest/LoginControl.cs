using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RealTime7Client.UpdateComponents;
using System.Net;
using System.IO;
using System.Web.Services.Protocols;
using RealTime7.Proxy;
using System.Threading;
using ADODB;
using RealTime7.Utilities;
using Microsoft.Win32;

namespace HardwareCompatibilityTest
{
    public partial class LoginControl : Form
    {
        Loginmsg getUser = new Loginmsg();
        private System.Windows.Forms.Timer loginTimer = new System.Windows.Forms.Timer();
                
        Recordset RecReceiptPrinter = null;
        Recordset RecInvoicePrinter = null;
        Recordset RecPoleDisplay = null;
        Recordset RecBarcodePrinter = null;
        Recordset RecAllTerminal = null;
        Recordset RecUnassignedTerminal = null;

        int statusCode;
        int storeID = -1;
                
        public LoginControl()
        {
            InitializeComponent();
            Control.CheckForIllegalCrossThreadCalls = false;
            this.loginTimer.Interval = 500;
        }
        
        private void btnOK_Click(object sender, EventArgs e)
        {
            if (this.txtLoginID.Text.Trim().Length >= 1 && this.txtPassword.Text.Trim().Length >= 1 && this.txtUserID.Text.Trim().Length >= 1)
            {
                if (this.textBoxURL.Text.Trim().Length >= 1)
                {
                    try
                    {
                        RegistryKey subKey = Registry.LocalMachine.CreateSubKey("SOFTWARE\\Mind The Store\\POS");
                        if (subKey == null)
                        {
                            int num = (int)MessageBox.Show("Failed to set registry setting.....Please login with administrator access.");
                            Application.Exit();
                            return;
                        }
                        
                        subKey.SetValue("Configured", "0");
                        subKey.Close();
                        this.btnOK.Enabled = false;
                        this.lblStatus.Text = "Requesting Login from the Server";
                        this.loginTimer.Tick += new EventHandler(this.loginTimer_Tick);
                        this.loginTimer.Start();

                        getUser.loginID = txtLoginID.Text.Trim();
                        getUser.userID = txtUserID.Text.Trim();
                        getUser.password = txtPassword.Text.Trim();

                        if (this.getUser.loginID.Length > 0 && this.getUser.userID.Length > 0)
                        {
                            UpdateComponent.InitComponent(this.textBoxURL.Text);
                            
                            UpdateComponent.RequestNewLoginEvent += new RequestNewLoginHandler(webService_RequestNewLoginEvent);
                            UpdateComponent.RequestNewLogin(getUser);
                        }
                        this.txtUserID.Focus();
                        return;
                    }
                    catch (WebException ex)
                    {
                        MessageBox.Show("Error occurred during Login......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                        Application.Exit();
                        return;
                    }
                    catch (FileNotFoundException ex)
                    {
                        MessageBox.Show("Error occurred during Login......Please reinstall the POS.\n(" + ex.Message + " Source:" + ex.Source + ")");
                        Application.Exit();
                        return;
                    }
                    catch (SoapException ex)
                    {
                        MessageBox.Show("Internal error occurred during Login....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                        Application.Exit();
                        return;
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Failed to login please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                        Application.Exit();
                        return;
                    }
                }
            }
            
            MessageBox.Show((IWin32Window)this, "Please Enter Required Fields", "Input Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
            
            if (this.textBoxURL.Text.Trim().Length < 1)
                this.textBoxURL.Focus();
            else if (this.txtLoginID.Text.Trim().Length < 1)
                this.txtLoginID.Focus();
            else if (this.txtUserID.Text.Trim().Length < 1)
            {
                this.txtUserID.Focus();
            }
            else
            {
                if (this.txtPassword.Text.Trim().Length >= 1)
                    return;
                this.txtPassword.Focus();
            }
        }

        void webService_RequestNewLoginEvent(object source, ResultEventArgs e)
        {
            try
            {                
                UpdateComponent.RequestNewLoginEvent -= webService_RequestNewLoginEvent;

                if (e.retVal.retCode != enmResult.Success)
                {                   
                    this.lblStatus.Text = "Login Failed";                    
                    MessageBox.Show(this, "Please Enter Proper Credentials");
                    this.txtPassword.Text = "";
                    this.txtLoginID.Focus();
                    this.btnOK.Enabled = true;
                }
                else
                {
                    UpdateComponent.RequestLoginEvent += new RequestLoginHandler(webService_RequestLoginEvent);
                    UpdateComponent.RequestLogin(this.getUser);
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error (code 2) occurred during Login......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (FileNotFoundException ex)
            {                
                MessageBox.Show("Error (code 2) occurred during Login......Please reinstall the POS.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Error (code 2) occurred during Login......Please reinstall the POS.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed (code 2) to login please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        void webService_RequestLoginEvent(object source, ResultLoginEventArgs e)
        {
            try
            {
                UpdateComponent.RequestLoginEvent -= webService_RequestLoginEvent;
                this.storeID = e.retVal.storeIDField;
                if (e.retVal.retCode != enmResult.Success)
                {
                    this.lblStatus.Text = "Login Failed";                    
                    MessageBox.Show(e.retVal.Remarks);
                    this.txtPassword.Text = "";
                    this.txtLoginID.Focus();
                    this.btnOK.Enabled = true;
                }
                else
                {
                    this.lblStatus.Text = "Login Successfull";

                    UpdateComponent.HWReceiptPrinterInfoEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWReceiptPrinterInfoEvent);
                    UpdateComponent.HWPoleDisplayInfoEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWPoleDisplayInfoEvent);
                    UpdateComponent.HWInvoicePrinterInfoEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWInvoicePrinterInfoEvent);
                    UpdateComponent.HWBarCodePrinterInfoEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWBarCodePrinterInfoEvent);
                    UpdateComponent.HWGetAllAssignedTerminalIDEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWGetAllAssignedTerminalIDEvent);
                    UpdateComponent.HWGetUnAssignedTerminalIDEvent += new RequestAnyRecordsetHandler(UpdateComponent_HWGetUnAssignedTerminalIDEvent);
                    UpdateComponent.HWReceiptPrinterInfo();
                    UpdateComponent.HWPoleDisplayInfo();
                    UpdateComponent.HWInvoicePrinterInfo();
                    UpdateComponent.HWBarCodePrinterInfo();
                    UpdateComponent.HWGetAllAssignedTerminalID();
                    UpdateComponent.HWGetUnAssignedTerminalID();
                }
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error (code 3) occurred during Login......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (FileNotFoundException ex)
            {
                MessageBox.Show("Error (code 3) occurred during Login......Please reinstall the POS.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error (code 3) occurred during Login....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed (code 3) to login please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        private void UpdateComponent_HWReceiptPrinterInfoEvent(object source, ResultRSEventArgs e)
        {
            try
            {
                UpdateComponent.HWReceiptPrinterInfoEvent -= UpdateComponent_HWReceiptPrinterInfoEvent;

                if (e.retVal.retCode == enmResult.Success)
                {
                    Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                    if (rsServerData.RecordCount > 0)
                    {
                        RecReceiptPrinter = rsServerData;
                    }
                    else
                    {
                        MessageBox.Show("No data available for the Receipt Printer.");
                    }
                }
                else
                {
                    MessageBox.Show("Failed to download the data of Receipt Printer....Please try after few moments. ");
                    Application.Exit();
                }
                ++this.statusCode;
                this.displaySatus(this.statusCode);
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred in downloading receipt Printer Information......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error occurred in downloading receipt Printer Information....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to download receipt Printer Information please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        private void UpdateComponent_HWPoleDisplayInfoEvent(object source, ResultRSEventArgs e)
        {
            try
            {
                UpdateComponent.HWPoleDisplayInfoEvent -= UpdateComponent_HWPoleDisplayInfoEvent;

                if (e.retVal.retCode == enmResult.Success)
                {
                    Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                    if (rsServerData.RecordCount > 0)
                    {
                        this.RecPoleDisplay = rsServerData;
                    }
                    else
                    {
                        MessageBox.Show("No data available for the Pole Display.");
                    }
                }
                else
                {
                    MessageBox.Show("Failed to download the data of Pole Display....Please try after few moments. ");
                    Application.Exit();
                }
                ++this.statusCode;
                this.displaySatus(this.statusCode);
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred in downloading customer display Information......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error occurred in downloading customer display Information....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to download customer display Information. please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        private void UpdateComponent_HWInvoicePrinterInfoEvent(object source, ResultRSEventArgs e)
        {
            try
            {                
                UpdateComponent.HWInvoicePrinterInfoEvent -= UpdateComponent_HWInvoicePrinterInfoEvent;

                if (e.retVal.retCode == enmResult.Success)
                {
                    Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                    if (rsServerData.RecordCount > 0)
                    {
                        this.RecInvoicePrinter = rsServerData;
                    }
                    else
                    {
                        MessageBox.Show("No data available for the Invoice printer.");
                    }
                }
                else
                {
                    MessageBox.Show("Failed to download the data of Invoice printer....Please try after few moments. ");
                    Application.Exit();
                }
                ++this.statusCode;
                this.displaySatus(this.statusCode);
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred in downloading invoice Printer Information......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error occurred in downloading invoice Printer Information....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to download invoice Printer Information please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        private void UpdateComponent_HWBarCodePrinterInfoEvent(object source, ResultRSEventArgs e)
        {
            try
            {
                UpdateComponent.HWBarCodePrinterInfoEvent -= UpdateComponent_HWBarCodePrinterInfoEvent;

                if (e.retVal.retCode == enmResult.Success)
                {
                    Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                    if (rsServerData.RecordCount > 0)
                    {
                        this.RecBarcodePrinter = rsServerData;
                    }
                    else
                    {
                        MessageBox.Show("No data available for the barcode printer.");
                    }
                }
                else
                {
                    MessageBox.Show("Failed to download the data of barcode printer....Please try after few moments. ");
                    Application.Exit();
                }
                ++this.statusCode;
                this.displaySatus(this.statusCode);
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred in downloading Barcode Printer Information......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error occurred in downloading Barcode Printer Information....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to download Barcode Printer Information please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        private void UpdateComponent_HWGetAllAssignedTerminalIDEvent(object source, ResultRSEventArgs e)
        {
            try
            {
                UpdateComponent.HWGetAllAssignedTerminalIDEvent -= UpdateComponent_HWGetAllAssignedTerminalIDEvent;

                if (e.retVal.retCode == enmResult.Success)
                {
                    Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                    if (rsServerData.RecordCount > 0)
                    {
                        this.RecAllTerminal = rsServerData;
                    }
                    else
                    {
                        MessageBox.Show("No data available for the Terminals.");
                    }
                }
                else
                {
                    MessageBox.Show("Failed to download the data of the Terminals....Please try after few moments. ");
                    Application.Exit();
                }
                ++this.statusCode;
                this.displaySatus(this.statusCode);
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred in downloading Terminal Information......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error occurred in downloading Terminal Information....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to download Terminal Information please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }

        private void UpdateComponent_HWGetUnAssignedTerminalIDEvent(object source, ResultRSEventArgs e)
        {
            try
            {
                UpdateComponent.HWGetUnAssignedTerminalIDEvent -= UpdateComponent_HWGetUnAssignedTerminalIDEvent;

                if (e.retVal.retCode == enmResult.Success)
                {
                    Recordset rsServerData = UpdateComponent.GetRS4String(e.retVal.xmlRS);
                    if (rsServerData.RecordCount > 0)
                    {
                        this.RecUnassignedTerminal = rsServerData;
                    }
                }
                else
                {
                    MessageBox.Show("Failed to download the data of the unassigned Terminal....Please try after few moments. ");
                    Application.Exit();
                }
                ++this.statusCode;
                this.displaySatus(this.statusCode);
            }
            catch (WebException ex)
            {
                MessageBox.Show("Error occurred in downloading Unassigned Terminal Information......Please check internet connection.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
            catch (SoapException ex)
            {
                MessageBox.Show("Internal error occurred in downloading Unassigned Terminal Information....Please try after few minutes. \n(" + ex.Message + " Source :" + ex.Source + "(");
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to download Unassigned Terminal Information please try after few minutes.\n(" + ex.Message + " Source:" + ex.Source + ")");
                Application.Exit();
            }
        }
        
        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult ret = MessageBox.Show(this, "Are You Sure You Want To Exit ?", "Exit", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
            if (ret == DialogResult.OK)
            {               
                Application.Exit();
            }
        }

        private void txtLoginID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, new EventArgs());
            }
            if (e.KeyCode == Keys.Enter)
            {
                txtUserID.Focus();
            }
        }


        private void txtUserID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, new EventArgs());
            }
            if (e.KeyCode == Keys.Enter)
            {
                txtPassword.Focus();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, new EventArgs());
            }
            if (e.KeyCode == Keys.Enter)
            {
                btnOK_Click(sender, new EventArgs());
            }
        }

        private void textBoxURL_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel_Click(sender, new EventArgs());
            }
            if (e.KeyCode == Keys.Enter)
            {
                txtLoginID.Focus();
            }
        }

        private void loginTimer_Tick(object sender, EventArgs e)
        {
            if (this.statusCode <= 5)
                return;
            this.loginTimer.Enabled = false;
            this.loginTimer.Dispose();
            this.Visible = false;
            TerminalDetails terminalDetails = new TerminalDetails(this.RecReceiptPrinter, this.RecInvoicePrinter, this.RecPoleDisplay, this.RecBarcodePrinter, this.RecAllTerminal, this.RecUnassignedTerminal);
            terminalDetails.ShowDialog();
            terminalDetails.Dispose();
            Application.Exit();
        }

        private void displaySatus(int code)
        {
            switch (code)
            {
                case 1:
                    this.lblStatus.Text = "Initialising download infomation database....";
                    break;
                case 2:
                    this.lblStatus.Text = "Initialising download infomation database....";
                    break;
                case 3:
                    this.lblStatus.Text = "Downloading store terminal information....";
                    break;
                case 4:
                    this.lblStatus.Text = "Downloading hardware information....";
                    break;
                case 5:
                    this.lblStatus.Text = "Download Complete....";
                    break;
            }
        }
    }
}