namespace HardwareCompatibilityTest
{
    partial class SaveTerminalConfiguration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelTerminalID = new System.Windows.Forms.Label();
            this.TextReceiptPrinter = new System.Windows.Forms.Label();
            this.textReceiptPrinterPort = new System.Windows.Forms.Label();
            this.textCustomerDisplay = new System.Windows.Forms.Label();
            this.textCustomerDisplayPort = new System.Windows.Forms.Label();
            this.textInvoicePrinter = new System.Windows.Forms.Label();
            this.textInvoicePrinterPort = new System.Windows.Forms.Label();
            this.comboReceiptPrinter = new System.Windows.Forms.ComboBox();
            this.comboBoxReceiptPrinterPort = new System.Windows.Forms.ComboBox();
            this.comboBoxCustomerDisplay = new System.Windows.Forms.ComboBox();
            this.comboBoxCustomerDisplayPort = new System.Windows.Forms.ComboBox();
            this.comboBoxInvoicePrinter = new System.Windows.Forms.ComboBox();
            this.comboBoxInvoicePrinterPort = new System.Windows.Forms.ComboBox();
            this.btnBack = new System.Windows.Forms.Button();
            this.SaveConfig = new System.Windows.Forms.Button();
            this.labelTextTerminalID = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTerminalID
            // 
            this.labelTerminalID.AutoSize = true;
            this.labelTerminalID.Location = new System.Drawing.Point(249, 88);
            this.labelTerminalID.Name = "labelTerminalID";
            this.labelTerminalID.Size = new System.Drawing.Size(0, 13);
            this.labelTerminalID.TabIndex = 0;
            // 
            // TextReceiptPrinter
            // 
            this.TextReceiptPrinter.AutoSize = true;
            this.TextReceiptPrinter.Location = new System.Drawing.Point(76, 136);
            this.TextReceiptPrinter.Name = "TextReceiptPrinter";
            this.TextReceiptPrinter.Size = new System.Drawing.Size(80, 13);
            this.TextReceiptPrinter.TabIndex = 1;
            this.TextReceiptPrinter.Text = "Receipt Printer:";
            // 
            // textReceiptPrinterPort
            // 
            this.textReceiptPrinterPort.AutoSize = true;
            this.textReceiptPrinterPort.Location = new System.Drawing.Point(76, 173);
            this.textReceiptPrinterPort.Name = "textReceiptPrinterPort";
            this.textReceiptPrinterPort.Size = new System.Drawing.Size(102, 13);
            this.textReceiptPrinterPort.TabIndex = 2;
            this.textReceiptPrinterPort.Text = "Receipt Printer Port:";
            // 
            // textCustomerDisplay
            // 
            this.textCustomerDisplay.AutoSize = true;
            this.textCustomerDisplay.Location = new System.Drawing.Point(76, 211);
            this.textCustomerDisplay.Name = "textCustomerDisplay";
            this.textCustomerDisplay.Size = new System.Drawing.Size(91, 13);
            this.textCustomerDisplay.TabIndex = 3;
            this.textCustomerDisplay.Text = "Customer Display:";
            // 
            // textCustomerDisplayPort
            // 
            this.textCustomerDisplayPort.AutoSize = true;
            this.textCustomerDisplayPort.Location = new System.Drawing.Point(76, 250);
            this.textCustomerDisplayPort.Name = "textCustomerDisplayPort";
            this.textCustomerDisplayPort.Size = new System.Drawing.Size(113, 13);
            this.textCustomerDisplayPort.TabIndex = 4;
            this.textCustomerDisplayPort.Text = "Customer Display Port:";
            // 
            // textInvoicePrinter
            // 
            this.textInvoicePrinter.AutoSize = true;
            this.textInvoicePrinter.Location = new System.Drawing.Point(76, 288);
            this.textInvoicePrinter.Name = "textInvoicePrinter";
            this.textInvoicePrinter.Size = new System.Drawing.Size(78, 13);
            this.textInvoicePrinter.TabIndex = 5;
            this.textInvoicePrinter.Text = "Invoice Printer:";
            // 
            // textInvoicePrinterPort
            // 
            this.textInvoicePrinterPort.AutoSize = true;
            this.textInvoicePrinterPort.Location = new System.Drawing.Point(76, 324);
            this.textInvoicePrinterPort.Name = "textInvoicePrinterPort";
            this.textInvoicePrinterPort.Size = new System.Drawing.Size(100, 13);
            this.textInvoicePrinterPort.TabIndex = 6;
            this.textInvoicePrinterPort.Text = "Invoice Printer Port:";
            // 
            // comboReceiptPrinter
            // 
            this.comboReceiptPrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboReceiptPrinter.FormattingEnabled = true;
            this.comboReceiptPrinter.Items.AddRange(new object[] {
            "Cash Drawer With No Receipt Printer Attached",
            "Default Printer",
            "Epson Printer",
            "Epson TM-T88III",
            "Generic Printer with no cash register",
            "ithaca POSJet 1000",
            "NCR 7194",
            "No Printer Attached",
            "Star Printer"});
            this.comboReceiptPrinter.Location = new System.Drawing.Point(219, 133);
            this.comboReceiptPrinter.Name = "comboReceiptPrinter";
            this.comboReceiptPrinter.Size = new System.Drawing.Size(218, 21);
            this.comboReceiptPrinter.TabIndex = 7;
            this.comboReceiptPrinter.SelectedIndexChanged += new System.EventHandler(this.comboReceiptPrinter_SelectedIndexChanged_1);
            // 
            // comboBoxReceiptPrinterPort
            // 
            this.comboBoxReceiptPrinterPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxReceiptPrinterPort.FormattingEnabled = true;
            this.comboBoxReceiptPrinterPort.Items.AddRange(new object[] {
            "Parallel Port (LPT1)",
            "Communications Port (COM1)",
            "Communications Port (COM2)",
            "USB Port"});
            this.comboBoxReceiptPrinterPort.Location = new System.Drawing.Point(219, 170);
            this.comboBoxReceiptPrinterPort.Name = "comboBoxReceiptPrinterPort";
            this.comboBoxReceiptPrinterPort.Size = new System.Drawing.Size(218, 21);
            this.comboBoxReceiptPrinterPort.TabIndex = 8;
            // 
            // comboBoxCustomerDisplay
            // 
            this.comboBoxCustomerDisplay.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCustomerDisplay.FormattingEnabled = true;
            this.comboBoxCustomerDisplay.Location = new System.Drawing.Point(219, 208);
            this.comboBoxCustomerDisplay.Name = "comboBoxCustomerDisplay";
            this.comboBoxCustomerDisplay.Size = new System.Drawing.Size(218, 21);
            this.comboBoxCustomerDisplay.TabIndex = 9;
            // 
            // comboBoxCustomerDisplayPort
            // 
            this.comboBoxCustomerDisplayPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxCustomerDisplayPort.FormattingEnabled = true;
            this.comboBoxCustomerDisplayPort.Items.AddRange(new object[] {
            "Parallel Port (LPT1)",
            "Communications Port (COM1)",
            "Communications Port (COM2)",
            "USB Port"});
            this.comboBoxCustomerDisplayPort.Location = new System.Drawing.Point(219, 247);
            this.comboBoxCustomerDisplayPort.Name = "comboBoxCustomerDisplayPort";
            this.comboBoxCustomerDisplayPort.Size = new System.Drawing.Size(218, 21);
            this.comboBoxCustomerDisplayPort.TabIndex = 10;
            // 
            // comboBoxInvoicePrinter
            // 
            this.comboBoxInvoicePrinter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInvoicePrinter.FormattingEnabled = true;
            this.comboBoxInvoicePrinter.Location = new System.Drawing.Point(219, 285);
            this.comboBoxInvoicePrinter.Name = "comboBoxInvoicePrinter";
            this.comboBoxInvoicePrinter.Size = new System.Drawing.Size(218, 21);
            this.comboBoxInvoicePrinter.TabIndex = 11;
            // 
            // comboBoxInvoicePrinterPort
            // 
            this.comboBoxInvoicePrinterPort.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxInvoicePrinterPort.FormattingEnabled = true;
            this.comboBoxInvoicePrinterPort.Items.AddRange(new object[] {
            "Parallel Port (LPT1)",
            "Communications Port (COM1)",
            "Communications Port (COM2)",
            "USB Port"});
            this.comboBoxInvoicePrinterPort.Location = new System.Drawing.Point(219, 321);
            this.comboBoxInvoicePrinterPort.Name = "comboBoxInvoicePrinterPort";
            this.comboBoxInvoicePrinterPort.Size = new System.Drawing.Size(218, 21);
            this.comboBoxInvoicePrinterPort.TabIndex = 12;
            // 
            // btnBack
            // 
            this.btnBack.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnBack.Location = new System.Drawing.Point(183, 368);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(108, 31);
            this.btnBack.TabIndex = 13;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = false;
            this.btnBack.Visible = false;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click_1);
            // 
            // SaveConfig
            // 
            this.SaveConfig.BackColor = System.Drawing.Color.CornflowerBlue;
            this.SaveConfig.Location = new System.Drawing.Point(327, 368);
            this.SaveConfig.Name = "SaveConfig";
            this.SaveConfig.Size = new System.Drawing.Size(110, 32);
            this.SaveConfig.TabIndex = 14;
            this.SaveConfig.Text = "Save";
            this.SaveConfig.UseVisualStyleBackColor = false;
            this.SaveConfig.Click += new System.EventHandler(this.SaveConfig_Click_1);
            // 
            // labelTextTerminalID
            // 
            this.labelTextTerminalID.AutoSize = true;
            this.labelTextTerminalID.Location = new System.Drawing.Point(76, 88);
            this.labelTextTerminalID.Name = "labelTextTerminalID";
            this.labelTextTerminalID.Size = new System.Drawing.Size(64, 13);
            this.labelTextTerminalID.TabIndex = 15;
            this.labelTextTerminalID.Text = "Terminal ID:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_Image;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(140, 53);
            this.pictureBox1.TabIndex = 16;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_BackgroundImage;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(0, 1);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(517, 53);
            this.pictureBox2.TabIndex = 17;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::HardwareCompatibilityTest.Properties.Resources.pictureBox1_BackgroundImage;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(0, 419);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(517, 54);
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            // 
            // SaveTerminalConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(517, 469);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelTextTerminalID);
            this.Controls.Add(this.SaveConfig);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.comboBoxInvoicePrinterPort);
            this.Controls.Add(this.comboBoxInvoicePrinter);
            this.Controls.Add(this.comboBoxCustomerDisplayPort);
            this.Controls.Add(this.comboBoxCustomerDisplay);
            this.Controls.Add(this.comboBoxReceiptPrinterPort);
            this.Controls.Add(this.comboReceiptPrinter);
            this.Controls.Add(this.textInvoicePrinterPort);
            this.Controls.Add(this.textInvoicePrinter);
            this.Controls.Add(this.textCustomerDisplayPort);
            this.Controls.Add(this.textCustomerDisplay);
            this.Controls.Add(this.textReceiptPrinterPort);
            this.Controls.Add(this.TextReceiptPrinter);
            this.Controls.Add(this.labelTerminalID);
            this.Name = "SaveTerminalConfiguration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SaveTerminalConfiguration";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelTerminalID;
        private System.Windows.Forms.Label TextReceiptPrinter;
        private System.Windows.Forms.Label textReceiptPrinterPort;
        private System.Windows.Forms.Label textCustomerDisplay;
        private System.Windows.Forms.Label textCustomerDisplayPort;
        private System.Windows.Forms.Label textInvoicePrinter;
        private System.Windows.Forms.Label textInvoicePrinterPort;
        private System.Windows.Forms.ComboBox comboReceiptPrinter;
        private System.Windows.Forms.ComboBox comboBoxReceiptPrinterPort;
        private System.Windows.Forms.ComboBox comboBoxCustomerDisplay;
        private System.Windows.Forms.ComboBox comboBoxCustomerDisplayPort;
        private System.Windows.Forms.ComboBox comboBoxInvoicePrinter;
        private System.Windows.Forms.ComboBox comboBoxInvoicePrinterPort;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button SaveConfig;
        private System.Windows.Forms.Label labelTextTerminalID;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}