using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using POSRECEIPTLib;
using BARCODE_PRJLib;
using ADODB;
namespace HardwareCompatibilityTest
{
    public partial class HardwareConfig : Form
    {
        PosPrint ObjPrint = new PosPrint();
        BarcodeAttrib ObjBarcode = new BARCODE_PRJLib.BarcodeAttrib();
        string[] strPorts = new string[4];
        public HardwareConfig(Recordset ReceiptPrintets, Recordset InvoicePrinters, Recordset PoleDisplay, Recordset BarcodePrinter)
        {
            try
            {
                InitializeComponent();
                strPorts[0] = "Parallel Port (LPT1)";
                strPorts[1] = "Communications Port (COM1)";
                strPorts[2] = "Communications Port (COM2)";
                strPorts[3] = "USB Port";
                comboBoxReceiptPrinterPort.DataSource = strPorts;
                comboBoxBarcodePrinterPort.DataSource = strPorts;
                comboBoxCustomerDisplayPort.DataSource = strPorts;
                comboBoxInvoicePrinterPort.DataSource = strPorts;



                if (ReceiptPrintets != null)
                {
                    string[] strReceiptPrinters = new string[ReceiptPrintets.RecordCount];
                    ReceiptPrintets.MoveFirst();
                    for (int i = 0; i < ReceiptPrintets.RecordCount; i++)
                    {
                        strReceiptPrinters[i] = ReceiptPrintets.Fields["PrinterName"].Value.ToString();
                        ReceiptPrintets.MoveNext();
                    }
                    //comboReceiptPrinter.DataSource = strReceiptPrinters;
                }
                if (InvoicePrinters != null)
                {
                    string[] strReceiptPrinters = new string[InvoicePrinters.RecordCount];
                    InvoicePrinters.MoveFirst();
                    for (int i = 0; i < InvoicePrinters.RecordCount; i++)
                    {
                        strReceiptPrinters[i] = InvoicePrinters.Fields["InvoicePrinterName"].Value.ToString();
                        InvoicePrinters.MoveNext();
                    }
                    comboBoxInvoicePrinter.DataSource = strReceiptPrinters;
                }
                if (PoleDisplay != null)
                {
                    string[] strReceiptPrinters = new string[PoleDisplay.RecordCount];
                    PoleDisplay.MoveFirst();
                    for (int i = 0; i < PoleDisplay.RecordCount; i++)
                    {
                        strReceiptPrinters[i] = PoleDisplay.Fields["CustomerDisplayName"].Value.ToString();
                        PoleDisplay.MoveNext();
                    }
                    comboBoxCustomerDisplay.DataSource = strReceiptPrinters;
                }
                if (BarcodePrinter != null)
                {
                    string[] strReceiptPrinters = new string[BarcodePrinter.RecordCount];
                    BarcodePrinter.MoveFirst();
                    for (int i = 0; i < BarcodePrinter.RecordCount; i++)
                    {
                        strReceiptPrinters[i] = BarcodePrinter.Fields["PrinterName"].Value.ToString();
                        BarcodePrinter.MoveNext();
                    }
                    comboBoxBarcodePrinter.DataSource = strReceiptPrinters;
                }
                
                comboReceiptPrinter.SelectedIndex = 7; //No Printter Attached
            }
            catch (Exception generalException)
            {
                MessageBox.Show("Caught general Exception in loading hardware config form." + generalException.Message + " Source:" + generalException.Source);
            }
        }

        private void HardwareConfig_Load(object sender, EventArgs e)
        {

        }

        private void BtnPrintSampleReceipt_Click_1(object sender, EventArgs e)
           {
            string strSampleReceipt;
            strSampleReceipt = "             Mind The Store|";
            strSampleReceipt += "       5775 Yonge Street 18th Fl|";
            strSampleReceipt += "           Toronto, Ont M2M 4J1|";
            strSampleReceipt += "                Canada|";
            strSampleReceipt += "             (416)226-9520|";
            strSampleReceipt += "|";
            strSampleReceipt += "              Sales Receipt|";
            strSampleReceipt += "4/2/2001         Trans No.         1-788|";
            strSampleReceipt += "3:19:37          CashierID       adminb1|";
            strSampleReceipt += "RegID 0033       SalesID             N/A|";
            strSampleReceipt += "----------------------------------------|";
            strSampleReceipt += "Item Number      Qty   Price         Ext|";
            strSampleReceipt += "----------------------------------------|";
            strSampleReceipt += "062823111585     1       4.99       4.99|";
            strSampleReceipt += "Mini Playing Card|";
            strSampleReceipt += "Color :Red";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "         Sub Total                  4.99|";
            strSampleReceipt += "              Tax1                  0.40|";
            strSampleReceipt += "              Tax2                  0.35|";
            strSampleReceipt += "        Sale Total                  5.74|";
            strSampleReceipt += "|";
            strSampleReceipt += "Tendered - CASH                     5.74|";
            strSampleReceipt += "Change Due                          0.00|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "========================================|";
            strSampleReceipt += "            Today's Special|";
            strSampleReceipt += "              BAR Chocolate|";
            strSampleReceipt += "            Buy 2 Get 1 Free|";
            strSampleReceipt += "========================================|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            strSampleReceipt += "|";
            switch (comboBoxReceiptPrinterPort.Text)
            {
                case "Parallel Port (LPT1)":
                    ObjPrint.PrinterPort = "LPT1:";
                    break;
                case "Communications Port (COM1)":
                    ObjPrint.PrinterPort = "COM1:";
                    break;
                case "Communications Port (COM2)":
                    ObjPrint.PrinterPort = "COM2:";
                    break;
                case "USB Port":
                    ObjPrint.PrinterPort = "USB:";
                    break;
                default:
                    MessageBox.Show("Please enter valid port.");
                    return;
            }

            int printerType = 0;

            switch (comboReceiptPrinter.Text)
            {
                /*
                     
                1   No Printer Attached                         0
                1   Generic Printer with no cash register       1
                2   Star Printer                                2 If USB:8
                3   ithaca POSJet 1000                          3
                4   Epson Printer                               4
                5   NCR 7194                                   7                               
                6   Epson TM-T88III                            11 If USB:9
                7   Default Printer                            10
                8   Cash Drawer With No Receipt Printer Attached 13
                     
                 */

                /*
                   
                 1   Cash Drawer With No Receipt Printer Attached
                 2   Default Printer
                 3   Epson Printer
                 4   Epson TM-T88III
                 5   Generic Printer with no cash register
                 6   ithaca POSJet 1000
                 7   NCR 7194
                 8   No Printer Attached
                 9   Star Printer
                 
                 */


                case "No Printer Attached":// No Printer Attached 
                    {
                        printerType = 0;
                        break;
                    }

                case "Generic Printer with no cash register"://Generic Printer with no cash register
                    {
                        printerType = 2;
                        break;
                    }
                case "Star Printer"://Star Printer 
                    {
                        if (comboBoxReceiptPrinterPort.Text == "USB Port")
                            printerType = 8;
                        else
                            printerType = 2;
                        break;
                    }

                case "ithaca POSJet 1000"://ithaca POSJet 1000 
                    {
                        printerType = 3;
                        break;
                    }

                case "Epson Printer"://Epson Printer  
                    {
                        printerType = 4;
                        break;
                    }

                case "NCR 7194":// NCR 7194  
                    {
                        printerType = 7;
                        break;
                    }

                case "Epson TM-T88III"://Epson TM-T88III 
                    {
                        if (comboBoxReceiptPrinterPort.Text == "USB Port")
                            printerType = 9;
                        else
                            printerType = 11;

                        break;
                    }

                case "Default Printer":// Default Printer 
                    {
                        printerType = 10;
                        break;
                    }

                case "Cash Drawer With No Receipt Printer Attached":
                    printerType = 13;
                    break;



                default:
                    printerType = 0;
                    break;
            }


            ObjPrint.PrinterType = printerType;
            ObjPrint.PrintContent = strSampleReceipt;
            ObjPrint.PrintReceipt();
        }

        private void btnDisplaySample_Click_1(object sender, EventArgs e)
        {

            ObjPrint.CustomerDisplayType = comboBoxCustomerDisplay.SelectedIndex;
            switch (comboBoxCustomerDisplayPort.Text)
            {
                case "Parallel Port (LPT1)":
                    ObjPrint.CustomerDisplayPort = "LPT1:";
                    break;
                case "Communications Port (COM1)":
                    ObjPrint.CustomerDisplayPort = "COM1:";
                    break;
                case "Communications Port (COM2)":
                    ObjPrint.CustomerDisplayPort = "COM2:";
                    break;
                case "USB Port":
                    ObjPrint.CustomerDisplayPort = "USB:";
                    break;
                default:
                    MessageBox.Show("Please enter valid port.");
                    return;
            }
            ObjPrint.CustomerDisplayPrint("Welcome To", "Mind The Store");
            MessageBox.Show(ObjPrint.CustomerDisplayType.ToString() + ObjPrint.CustomerDisplayPort.ToString());
        }

        private void btnPrintInvoiceSample_Click_1(object sender, EventArgs e)
         {
            int retcode = 0;
            string strSampleInvoice = "";
            if (comboBoxInvoicePrinter.SelectedIndex == 1)
            {
                strSampleInvoice = "                                Mind The Store|" +
                                   "                          5775 Yonge Street 18th Fl|" +
                                   "                            Toronto, Ont M2M 4J1|" +
                                   "                                    Canada|" +
                                   "                                 (416)226-9520|" +
                                   "|" +
                                   "                                  Sales Receipt|" +
                                   "4/2/2001                                                 Trans No.         1-788|" +
                                   "3:19:37                                                  CashierID       adminb1|" +
                                   "RegID 0033                                               SalesID             N/A|" +
                                   "--------------------------------------------------------------------------------|" +
                                   "Item Number      Description                             Qty   Price         Ext|" +
                                   "--------------------------------------------------------------------------------|" +
                                   "062823111585     Mini Playing Card                       1       4.99       4.99|" +
                                   "Color :Red|" +
                                   "|" +
                                   "|" +
                                   "                                                 Sub Total                  4.99|" +
                                   "                                                      Tax1                  0.40|" +
                                   "                                                      Tax2                  0.35|" +
                                   "                                                Sale Total                  5.74|" +
                                   "|" +
                                   "                                        Tendered - CASH                     5.74|" +
                                   "                                        Change Due                          0.00|" +
                                   "|" +
                                   "|" +
                                   "|" +
                                   "================================================================================|" +
                                   "                                Today's Special|" +
                                   "                                  BAR Chocolate|" +
                                   "                                Buy 2 Get 1 Free|" +
                                   "================================================================================|";
                switch (comboBoxInvoicePrinterPort.Text)
                {
                    case "Parallel Port (LPT1)":
                        ObjPrint.InvoicePrinterPort = "LPT1:";
                        break;
                    case "Communications Port (COM1)":
                        ObjPrint.InvoicePrinterPort = "COM1:";
                        break;
                    case "Communications Port (COM2)":
                        ObjPrint.InvoicePrinterPort = "COM2:";
                        break;
                    case "USB Port":
                        ObjPrint.InvoicePrinterPort = "USB:";
                        break;
                    default:
                        MessageBox.Show("Please enter valid port.");
                        return;
                }
                ObjPrint.InvoicePrinterType = 1;
                retcode = ObjPrint.get_PrintInvoice(strSampleInvoice);
            }
            else if (comboBoxInvoicePrinter.SelectedIndex == 2)
            {
                string strIn;
                strIn = "~sn~REALTIME7 INC~^sn~";
                strIn += "~a1~869 Gana Court.~^a1~";
                strIn += "~a2~Mississauga, Ontario~^a2~";
                strIn += "~a3~Canada, L5S 1N9~^a3~";
                strIn += "~a4~(905) 670-5704~^a4~";
                strIn += "~sr~SALES RECEIPT~^sr~";
                strIn += "~dt~Date: 01-April-2005~^dt~";
                strIn += "~tn~Trans No: 1-1148~^tn~";
                strIn += "~tm~Time: 06:30:08~^tm~";
                strIn += "~cd~Cashier ID: Admintor~^cd~";
                strIn += "~rd~REGID 0003~^rd~";
                strIn += "~sd~Sales ID: N/A~^sd~";
                strIn += "~ci~Sold To: Ganesh Lande (007) PH #: 888-444-2222~^ci~";
                strIn += "~na~777.88~^na~";
                strIn += "~t1~12.12~^t1~";
                strIn += "~x1~11.11~^x1~";
                strIn += "~t2~11.55~^t2~";
                strIn += "~x2~88.55~^x2~";
                strIn += "~ta~66.22~^ta~";
                strIn += "~tc~2201.00~^tc~";
                strIn += "~tv~11.22~^tv~";
                strIn += "~cu~11.00~^cu~";
                strIn += "~ch~00.00~^ch~";
                strIn += "~e1~Tax1 Exempt:254.12~^e1~";
                strIn += "~e2~Tax2 Exempt:33.25~^e2~";
                strIn += "~c1~Thank you for shopping with us~^c1~";
                strIn += "~c2~No Refunds or Exchanges after 7 days.~^c2~";
                strIn += "~c3~Visit Again.~^c3~";
                strIn += "~gs~TAX # 123456~^gs~";
                strIn += "~sg~VISA CardNo. xxxxxxxxxxx5648~^sg~";
                strIn += "~sg~Exp 05/07 Auth. No. 0151-000445~^sg~";
                strIn += "~sg~I AGREE TO PAY ABOVE TOTAL AMOUNT~^sg~";
                strIn += "~sg~ACCORDING TO CARD ISSUER AGREEMENT~^sg~";
                strIn += "~sg~X___________________________~^sg~";
                for (long i = 0; i < 2; i++)
                {
                    strIn += "~ts~~ii~110~^ii~~qt~2~^qt~~dc~Zebra LP2844 printer.~^dc~~ut~1.2~^ut~~up~22.5~^up~~la~50.12~^la~~^ts~";
                }
                strIn += "~ts~~ii~Service Charges~^ii~~^ts~";
                strIn += "~ts~~dc~PACKING~^dc~~la~78.56~^la~~^ts~";
                strIn += "~ts~~dc~Shipping~^dc~~la~10.23~^la~~^ts~";
                ObjPrint.PrintFormattedReceipt(strIn);
            }

        }

        private void btnPrintBarcode_Click_1(object sender, EventArgs e)
         {
            switch (comboBoxBarcodePrinterPort.Text)
            {
                case "Parallel Port (LPT1)":
                    ObjBarcode.BarCodePrinterPort = "LPT1:";
                    break;
                case "Communications Port (COM1)":
                    ObjBarcode.BarCodePrinterPort = "COM1:";
                    break;
                case "Communications Port (COM2)":
                    ObjBarcode.BarCodePrinterPort = "COM2:";
                    break;
                case "USB Port":
                    ObjBarcode.BarCodePrinterPort = "USB:";
                    break;
                default:
                    MessageBox.Show("Please enter valid port.");
                    return;
            }
            ObjBarcode.BarCodePrinterType = comboBoxBarcodePrinter.SelectedIndex;
            int res1 = ObjBarcode.get_BarCode_Height("1.25*1", "90");
            int res = ObjBarcode.get_PrintBarCode("Test", "", "Description", "", "", (float)3.4, 1);
            if (res != 0)
                MessageBox.Show("Error In Printing ");
        }

        private void btnBack_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

      

    }
}