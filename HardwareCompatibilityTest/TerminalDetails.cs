using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using RealTime7Client.UpdateComponents;
using ADODB;
using System.Net;
using System.Web.Services.Protocols;
using System.IO;
namespace HardwareCompatibilityTest
{
    public partial class TerminalDetails : Form
    {
        
        Recordset UnassignedTerminallist = null;
        Recordset AllTerminalList = null;
        SaveTerminalConfiguration SaveConfigForm;
        HardwareConfig HardwareConfigForm;
        public TerminalDetails(Recordset ReceiptPrintets, Recordset InvoicePrinters, Recordset PoleDisplay, Recordset BarcodePrinters, Recordset RecAllTerminal, Recordset RecUnassignedTerminal)
        {
            try
            {
                AllTerminalList = RecAllTerminal;
                UnassignedTerminallist = RecUnassignedTerminal;
                SaveConfigForm = new SaveTerminalConfiguration(ReceiptPrintets, InvoicePrinters, PoleDisplay);
                HardwareConfigForm = new HardwareConfig(ReceiptPrintets, InvoicePrinters, PoleDisplay, BarcodePrinters);
                InitializeComponent();

                if (RecUnassignedTerminal != null)
                {
                    string[] srtTerminalIDs = new string[RecUnassignedTerminal.RecordCount];
                    RecUnassignedTerminal.MoveFirst();
                    for (int i = 0; i < RecUnassignedTerminal.RecordCount; i++)
                    {
                        srtTerminalIDs[i] = RecUnassignedTerminal.Fields["TerminalID"].Value.ToString();
                        RecUnassignedTerminal.MoveNext();
                    }
                    comboBoxTerminalID.DataSource = srtTerminalIDs;
                }
                else
                {
                    string[] srtTerminalIDs = new string[1];
                    srtTerminalIDs[0] = "N/A";
                    comboBoxTerminalID.DataSource = srtTerminalIDs;
                }
            }
            catch (Exception generalException)
            {
                MessageBox.Show("Error in loading hardware config form." + generalException.Message + " Source:" + generalException.Source);
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }

        }

        void UpdateComponent_RequestTerminalInfoEvent(object source, ResultDSEventArgs e)
        {
            try
            {
                UpdateComponent.RequestTerminalInfoEvent -= UpdateComponent_RequestTerminalInfoEvent;
                ADODB.Recordset rsHardwareConfig = UpdateComponent.DS2RS(e.retVal.dsData.Tables[0]);
                labelDownloadingData.Visible = false;
                SaveConfigForm.setStoreIDAndDefaultConfiguration(comboBoxTerminalID.Text, rsHardwareConfig);
                //this.Visible = false;
                SaveConfigForm.ShowDialog();
                //this.Visible = true;
                this.Close();
            }
            catch (WebException webexptn)
            {
                MessageBox.Show("Error occurred during  Terminal Information download.......Please check internet connection.\n(" + webexptn.Message + " Source:" + webexptn.Source + ")");
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }
            catch (SoapException soapexcptn)
            {
                MessageBox.Show("Internal error occurred during Terminal Information download.....Please try after few minutes. \n(" + soapexcptn.Message + " Source :" + soapexcptn.Source + "(");
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }
            catch (Exception generalException)
            {
                MessageBox.Show("Terminal Information download Failed....please try after few minutes.\n(" + generalException.Message + " Source:" + generalException.Source + ")");
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }
        }


        private void ReplaceTerminal_Click_1(object sender, EventArgs e)
        {

            if (ReplaceTerminal.Text == "Show all POS terminals")
            {
                if (AllTerminalList != null)
                {
                    string[] srtTerminalIDs = new string[AllTerminalList.RecordCount];
                    AllTerminalList.MoveFirst();
                    for (int i = 0; i < AllTerminalList.RecordCount; i++)
                    {
                        srtTerminalIDs[i] = AllTerminalList.Fields["TerminalID"].Value.ToString();
                        AllTerminalList.MoveNext();
                    }
                    comboBoxTerminalID.DataSource = srtTerminalIDs;
                }
                else
                {
                    string[] srtTerminalIDs = new string[1];
                    srtTerminalIDs[0] = "N/A";
                    comboBoxTerminalID.DataSource = srtTerminalIDs;
                }
                ReplaceTerminal.Text = "Show unassigned terminals only";
                groupBoxTerminalConfig.Text = "All Terminals";
            }
            else
            {
                if (UnassignedTerminallist != null)
                {
                    string[] srtTerminalIDs = new string[UnassignedTerminallist.RecordCount];
                    UnassignedTerminallist.MoveFirst();
                    for (int i = 0; i < UnassignedTerminallist.RecordCount; i++)
                    {
                        srtTerminalIDs[i] = UnassignedTerminallist.Fields["TerminalID"].Value.ToString();
                        UnassignedTerminallist.MoveNext();
                    }
                    comboBoxTerminalID.DataSource = srtTerminalIDs;
                }
                else
                {
                    string[] srtTerminalIDs = new string[1];
                    srtTerminalIDs[0] = "N/A";
                    comboBoxTerminalID.DataSource = srtTerminalIDs;
                }
                ReplaceTerminal.Text = "Show all POS terminals";
                groupBoxTerminalConfig.Text = "Unassigned terminals";
            }

        }

        private void compatibilitytestOnhardware_Click_1(object sender, EventArgs e)
        {
            //this.Visible = false;
            HardwareConfigForm.ShowDialog();
            //this.Visible = true;
        }

        private void btnShowTerminalDetails_Click_1(object sender, EventArgs e)
        {
            //clsRegistry Objregistry = new clsRegistry();
            //Objregistry.CreateSubKey(Microsoft.Win32.Registry.LocalMachine, @"SOFTWARE\Mind The Store\POS");
            //Microsoft.Win32.Registry.LocalMachine.OpenSubKey("SOFTWARE").CreateSubKey("ganesh"); 
            try
            {
                if (comboBoxTerminalID.Text == "" || comboBoxTerminalID.Text == "N/A")
                {
                    MessageBox.Show("Please Select Proper TerminalID");
                    return;
                }
                labelDownloadingData.Text = "Downloading data from the server.Please wait....";
                labelDownloadingData.Visible = true;

                UpdateComponent.RequestTerminalInfoEvent += new RequestDataSetHandler(UpdateComponent_RequestTerminalInfoEvent);
                UpdateComponent.RequestTerminalInfo(comboBoxTerminalID.Text);
               
            }
            catch (WebException webexptn)
            {
                MessageBox.Show("Error occurred in downloading Terminal Information.......Please check internet connection.\n(" + webexptn.Message + " Source:" + webexptn.Source + ")");
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }
            catch (SoapException soapexcptn)
            {
                MessageBox.Show("Internal error occurred in downloading Terminal Information.....Please try after few minutes. \n(" + soapexcptn.Message + " Source :" + soapexcptn.Source + "(");
                this.Close();
                //System.Windows.Forms.Application.Exit();
            }
            catch (Exception generalException)
            {
                MessageBox.Show("Failed to download Terminal Information please try after few minutes.\n(" + generalException.Message + " Source:" + generalException.Source + ")");
               this.Close();
               // System.Windows.Forms.Application.Exit();
            }
           

        }

        private void labelDownloadingData_Click(object sender, EventArgs e)
        {
           
        }

        private void TerminalDetails_Load(object sender, EventArgs e)
        {
          
        }
    }
}